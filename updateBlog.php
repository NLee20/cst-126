<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 3/19/17
 * Time: 8:26 PM
 */
require_once "utility.php";
include "header.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Update Blog</title>
</head>
<style>
    body{
        background-color: #7bb1cc;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 10px 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<!--

-->
<form action="updateHandler.php?ID=<?=$_GET["ID"]?>" method="post">

    Title:
    <br>
    <input type="text" name="Title" minlength="5" maxlength="50"/>
    <br>
    Blog Entry:
    <br>
    <textarea name="Entry" rows="5" cols="10" maxlength="1000"/>
    </textarea>
    <br>
    <input name="Submit" type="submit"/>
</form>

<br>
<br>
<br>
</body>
</html>