<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 4/9/17
 * Time: 4:05 PM
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Error</title>
</head>
<style>
    body{
        background-color: #7bb1cc;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 10px 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<?php
include 'header.php';
?>

<h2><?php echo $message?></h2><br>

</body>

</html>
