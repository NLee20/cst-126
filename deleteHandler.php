<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 3/19/17
 * Time: 8:47 PM
 */
require_once "utility.php";
include "header.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Blog Deleted</title>
</head>
<style>
    body{
        background-color: #7bb1cc;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 10px 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<p>Blog deleted successfully!</p>
<?php
$userId = getUserId();
$blogId = $_GET["ID"];
$conn = dbconnect();
$sql = "DELETE FROM blog_entry
WHERE ID = $blogId";

//execute query
$result = $conn->query($sql);

if($result -> mysqli_affected_rows() == 0)
{

}
elseif ($result -> mysqli_affected_rows() == 1)
{

}
else
{

}
//use the mysqli_affected_rows()

$conn->close();
?>
</body>
</html>