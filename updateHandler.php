<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 3/19/17
 * Time: 9:24 PM
 */
require_once "utility.php";
include "header.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Updated</title>
</head>
<style>
    body{
        background-color: #7bb1cc;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 10px 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>


<?php

$title = $_POST["Title"];
$entry = $_POST["Entry"];
$userId = getUserId();
$blogId = $_GET["ID"];

if($title == NULL || trim($title) == "")
{
    $message = "Title required.";
    include('_blogPostFail.php');
}
elseif($entry == NULL || trim($entry) == "")
{
    $message = "Entry required.";
    include('_blogPostFail.php');
}


$conn = dbConnect();

$sql = "UPDATE blog_entry SET TITLE = '" . $title . "', ENTRY = '" . $entry . "' WHERE ID =  '" . $blogId . "'";

//check for number of rows updated
//use the mysqli_affected_rows()

$result = $conn->query($sql);

if(mysqli_affected_rows($conn) > 0){
    $message = "Blog updated successfully!";
    include('_blogResponse.php');
}
else{
    $message = "Error: " . $sql . "<br>" . $conn->error;
    include('_blogPostFail.php');
}
$conn->close();
?>
</body>
</html>