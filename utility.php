<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 2/10/17
 * Time: 1:19 PM
 */
function dbConnect()
{
    //connect to database
    $dbservername = "localhost";
    $dbusername = "nleebhos_400IM";
    $dbpassword = "legoman27";
    $dbname = "nleebhos_Blogging";

    $conn = new mysqli($dbservername, $dbusername, $dbpassword, $dbname);

//check connection
    if($conn->connect_error)
    {
        die("Connection failed: " .$conn->connect_error);
    }
    //return connection

    return $conn;
}


function saveUserId($id)
{
    $_SESSION["USER_ID"] = $id;
    $_SESSION["LOGGED_IN"] = true;
}

function getUserId()
{
    return $_SESSION["USER_ID"];
}

function getLoggedIn()
{
    return $_SESSION["LOGGED_IN"];
}

function search($pattern)
{
    //connect to db
    $conn = dbConnect();

    $sql = "SELECT ID, TITLE FROM blog_entry WHERE TITLE LIKE '%$pattern%'";
    $result = $conn->query($sql);

    if($result->num_rows == 0)
    {
        return null;
    }

    else {
        $index = 0;
        $blogs = array();
        while ($row = $result->fetch_assoc()) {
            $blogs[$index] = array($row["ID"], $row["TITLE"]);
            ++$index;
        }

        $conn->close();

        return $blogs;
    }
}

function listBlogs()
{
    ?>

<table>


    <?php
    $conn = dbConnect();
    $sql = "SELECT ID, TITLE FROM blog_entry";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()){
        ?>
        <tr>
            <td>
                <a href="blogPage.php?id=<?=$row["ID"]?>"><?=$row["TITLE"]?></a>
            </td>
        </tr>
        <?php
    }
    $conn->close();
    ?>


</table>
<?php
}

function listComments($blogId)
{
    $conn = dbConnect();
    $sql = "SELECT users.USERNAME, comments.COMMENT, comments.RATING FROM comments, users
 WHERE users.ID = comments.USER_ID AND comments.BLOG_ID = ". $blogId;
    $result = $conn->query($sql);

    $index = 0;
    $comments = array();
    while($row = $result->fetch_assoc()) {
        $comments[$index] = array($row["USERNAME"], $row["COMMENT"], $row["RATING"]);
        ++$index;
    }
    ?>

    <table>


        <?php
        $index = 0;
        while($index < count($comments)){
            ?>
            <tr>
                <td>
                    <?php
                    echo $comments[$index][0] . "<br>" . $comments[$index][1] . "<br>Rating: " . $comments[$index][2];
                    $index++;
                    ?>
                </td>
            </tr>
            <?php
        }
        $conn->close();
        ?>


    </table>
    <?php

}

?>