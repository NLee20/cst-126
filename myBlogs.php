<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 3/19/17
 * Time: 6:03 PM
 */
require_once "utility.php";
include "header.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>My Blogs</title>
</head>
<style>
    body{
        background-color: #7bb1cc;
        align-content: center;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 10px 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<table>
<?php
$userId = getUserId();
$title = $_GET["TITLE"];
$blogId = $_GET["ID"];

$conn = dbConnect();
$sql = "SELECT ID, TITLE FROM blog_entry WHERE USER_ID=$userId";
$result = $conn->query($sql);
while($row = $result->fetch_assoc()){
    ?>
    <tr>
        <td>
            <a href="blogPage.php?id=<?=$row["ID"]?>"><?=$row["TITLE"]?></a>
            <a href="updateBlog.php?ID=<?=$row["ID"]?>">Update</a>
            <a href="deleteBlog.php?ID=<?=$row["ID"]?>">Delete</a>
        </td>
    </tr>
    <?php
}
$conn->close();
?>
</table>

</body>
</html>
