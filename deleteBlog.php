<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 3/19/17
 * Time: 8:26 PM
 */
require_once "utility.php";
include "header.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Delete Blog</title>
</head>
<style>
    body{
        background-color: #7bb1cc;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 10px 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<br>
<h3>
Blog Deleted
</h3>
<?php
$conn = dbconnect();
$blogId = $_GET["ID"];
$userID = mysqli_query($conn, "SELECT USER_ID FROM blog_entry WHERE ID = ". $blogId);
$userID = $userID->fetch_assoc();
if(getUserId() == $userID["USER_ID"]){
    $deleteComments = mysqli_query($conn, "DELETE FROM comments WHERE BLOG_ID = ". $blogId);
    $sql = "DELETE FROM blog_entry
WHERE ID = ".$blogId;
    $conn->query($sql);

}
$conn->close();
?>
</body>
</html>