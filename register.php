<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Registration Form</title>
</head>
<style>
    body{
        background-color: #7bb1cc;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 10px 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<?php
require_once 'utility.php';
include 'header.php';
?>
<form action="_registerHandler.php" method="post">
    First Name<br>
    <input name="FirstName" type="text" maxlength="50" minlength="3"/>(min 3 characters)<br>
    Last Name<br>
    <input name="LastName" type="text" maxlength="50" minlength="3"/>(min 3 characters)<br>
    Username<br>
    <input name="Username" type="text" maxlength="50" minlength="5"/>(min 5 characters)<br>
    Password<br>
    <input name="Password" type="password" maxlength="50" minlength="8"/>(min 8 characters)<br>
    E-mail<br>
    <input name="Email" type="email" maxlength="100"/><br>
    Age<br>
    <input name="Age" type="number" /><br>
    <input name="Submit" type="submit"/>
</form><br><br>

</body>
</html>