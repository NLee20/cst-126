<?php
require_once 'utility.php';
include 'header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Search</title>
</head>
<style>
    body{
        background-color: #7bb1cc;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 10px 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<form action="searchHandler.php" method="post">
    <table>
        <tr>
            <td>Search title: </td><td><input name="pattern" type="text"></td>
        </tr>
        <tr>
            <td colspan="2" align="center"><input name="Login" value="Search" type="submit"></td>
        </tr>
    </table>
</form>
<br><br>

</body>
</html>