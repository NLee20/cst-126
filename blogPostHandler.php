
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Post Successful</title>
</head>
<style>
    body{
        background-color: #7bb1cc;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 10px 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<?php

require_once "utility.php";
include "header.php";



$title = $_POST["Title"];
$entry = $_POST["Entry"];
$userId = getUserId();

if($title == NULL || trim($title) == "")
{
    $message = "Title required.";
    include('_blogPostFail.php');
}
elseif($entry == NULL || trim($entry) == "")
{
    $message = "Entry required.";
    include('_blogPostFail.php');
}

else {
    $conn = dbConnect();

    $sql = "INSERT INTO blog_entry (TITLE, ENTRY, USER_ID) VALUES ('" . $title . "' , '" . $entry . "' , '" . $userId . "')";

    if ($conn->query($sql) == TRUE) {
        $message = "Blog submitted successfully!";
        include('_blogResponse.php');
    } else {
        $message = "Error: " . $sql . "<br>" . $conn->error;
        include('_blogPostFail.php');
    }
    $conn->close();
}
?>
<br>

</body>
</html>