<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 4/9/17
 * Time: 3:29 PM
 */
require_once 'utility.php';
//include 'header.php';
session_start();

$comment = $_POST["Comment"];
$rating = $_POST["rating"];
$blogId = $_GET["id"];

if($comment == NULL || trim($comment) =="")
{
    $message="Comment is a required field";
    include 'error.php';
}
elseif($rating == 0)
{
    $message="Rating is required";
    include 'error.php';
}
else {
    $conn = dbConnect();

    $sql = "INSERT INTO comments (USER_ID, BLOG_ID, COMMENT, RATING) VALUES (".getUserId().", ". $blogId .",
    '" . $comment . "', '" . $rating . "')";

    if ($conn->query($sql) == true) {
        //$message = "Comment made!";
        header("Location: blogPage.php?id=" . $blogId);

    } else {
        $message = "Error: " . $sql . "<br>" . $conn->error;
        include 'error.php';
    }


    $conn->close();
}
?>