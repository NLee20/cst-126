<?php
require_once 'utility.php';
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 3/27/17
 * Time: 1:24 PM
 */
//include 'header.php';
?>

<style>
    body{
        background-color: #7bb1cc;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 10px 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>

<?
$pattern = $_POST["pattern"];

$blogs = search($pattern);

if($blogs != null) {
    include('_listBlogs.php');
    echo "Unable to process request at this time. Please try again later.";
    header("Location: index.php");
}

else
    echo "<b> There are no blogs with that title. </b>";
?>

