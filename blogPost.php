<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>New Post</title>
</head>
<style>
    body{
        background-color: #7bb1cc;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 10px 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<?php
require_once 'utility.php';
include 'header.php';
?>
<form action="blogPostHandler.php" method="post">
    Title:
    <br>
    <input type="text" name="Title" minlength="5" maxlength="50"/>
    <br>
    Blog Entry:
    <br>
    <textarea name="Entry" rows="5" cols="100" minlength="10" maxlength="1000"/>
    </textarea>
    <br>
    <input name="Submit" type="submit"/>
</form>

</body>
</html>