-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 05, 2017 at 11:44 PM
-- Server version: 5.6.33
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nleebhos_Blogging`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_entry`
--

CREATE TABLE IF NOT EXISTS `blog_entry` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TITLE` varchar(50) NOT NULL,
  `ENTRY` varchar(1000) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `blog_entry`
--

INSERT INTO `blog_entry` (`ID`, `DATE`, `TITLE`, `ENTRY`, `USER_ID`) VALUES
(1, '0000-00-00 00:00:00', 'I Am SuperMan!', '', 0),
(2, '2017-02-20 00:09:57', 'Will This Work?', '', 0),
(3, '2017-02-20 00:11:34', 'Haza!', '', 0),
(4, '2017-02-20 00:31:47', 'Alakazam', '', 0),
(5, '2017-02-22 19:35:06', 'Hello Masters.', '', 0),
(6, '2017-02-22 20:11:44', 'Hello.', '', 0),
(7, '2017-02-22 20:24:55', 'Test Entry.', '', 0),
(8, '2017-02-22 20:27:13', 'lol.', '', 0),
(9, '2017-02-24 20:11:46', 'Hello', 'My name is Nate.    ', 0),
(10, '2017-02-27 00:40:27', 'Topic 1', 'Hello, I am a creature from a very distant planet. The people here are strange. They like to "socialize" with "their friends," it is a very strange thing. They take part in many activities, including something they call "pool." This involves no water, only balls, sticks, and a tables. The balls are different colors and are meant to be hit into different holes in the table.    ', 24),
(11, '2017-03-03 20:29:25', 'Test1', '    This is only a test for the user being logged in.', 44),
(12, '2017-03-03 20:29:48', 'Test2', '    This entry should be the same user as the previous.', 44),
(13, '2017-03-05 23:49:53', 'Test4', '    Hello, This is a test of the updated website.', 44);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(100) NOT NULL,
  `LAST_NAME` varchar(100) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `AGE` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FIRST_NAME`, `LAST_NAME`, `USERNAME`, `PASSWORD`, `EMAIL`, `AGE`) VALUES
(24, 'Nathaniel', 'Lee', '123', '456', 'blackrook27@gmail.com', 12),
(25, 'Nate', 'Lee', '400IM', 'lol123', 'lol@gmail.com', 18),
(27, 'Bruce', 'Lee', 'BL13', 'lol', 'bl@gmail.com', 37),
(28, 'Chan', 'Saephan', 'Ay', '123', 'ay@gmail.com', 19),
(29, 'Cory', 'Cathrea', 'CC', '1234', 'cc@gmail.com', 19),
(32, 'Nathaniel', 'Lee', 'lol', '123', 'blackrook27@gmail.com', 10),
(33, 'l', 'd', '1', '2', 'aaa', 21),
(34, 'h', 'i', 'lol2.0', 'i', 'i', 1),
(35, 'Nate', 'Lee', 'lol123', '987654321', 'haha21@gmail.com', 33),
(36, 'joe', 'maemone', 'lol', 'weight', '99@gmail.com', 45),
(37, 'Blake', 'Wilson', '123456', 'haha', 'bw33@gmail.com', 21),
(38, 'Lathan', 'Lam', 'xLathan', '2143', 'xL@gmail.com', 22),
(39, 'Lathan', 'Lam', 'xLathan', '2413', 'xL@gmail.com', 22),
(40, 'Jake', 'Dibly', 'JD13', 'texas', 'jd12@gmail.com', 20),
(41, 'Asian', 'Man', '5521', '0987654321', 'lol0987@gmail.com', 19),
(42, '12345', '67890', '9134', '1234567', 'hghg@gmail.com', 21),
(43, '1234567890', '0987654321', '1234567890', '0987654321`', 'qwerty@gmail.com', 23),
(44, 'Nate', 'Lee', '200IM', 'swimming', '123@gmail.com', 21);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
