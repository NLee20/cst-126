<?php
require_once 'myfuncs.php';
include 'header.php';

/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 1/30/17
 * Time: 12:52 PM
 */
//variables
$username = $_POST["username"];
$password = $_POST["password"];

//validate data
if($username == NULL || trim($username) == "")
{
    $message = "Username required.";
    include ('loginFailed.php');
}
if($password == NULL || trim($password) == "")
{
    $message = "Password required.";
    include ('loginFailed.php');
}

//connect to database
$conn = dbConnect();

//select username and password from database
$sql = "SELECT ID, USERNAME, PASSWORD
        FROM users WHERE " . " BINARY USERNAME='" . $username . "' AND " . " BINARY PASSWORD='" . $password . "'";
$result = $conn ->query($sql);
if($conn->error)
{
    $message = "Error: " . $sql . "<br>" . $conn->error;
    include('loginFailed.php');
}
elseif($result->num_rows == 1)
{
    $row = $result->fetch_assoc();
    saveUserId($row["ID"]);
    $message = "Login was successful.";
    include('loginResponse.php');
}
elseif($result->num_rows == 0)
{
    $message = "Login Failed.";
    include('loginFailed.php');
}
elseif($result->num_rows > 1)
{
    $message = "There are multiple users registered.";
    include('loginFailed.php');
}
else
{
    $message = "Unknown Error.";
    include('loginFailed.php');
}

//close connection to database
$conn->close();
?>