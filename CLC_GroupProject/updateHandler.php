<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 3/19/17
 * Time: 9:24 PM
 */
require_once "myfuncs.php";
include "header.php";
include "_menu.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Updated</title>
</head>
<style>
    body{
        background-color: darkorchid;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 14px 25px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>


<?php

$title = $_POST["Title"];
$entry = $_POST["Entry"];
$userId = getUserId();
$blogId = $_GET["ID"];

if($title == NULL || trim($title) == "")
{
    $message = "Title required.";
    include('blogPostFail.php');
}
if($entry == NULL || trim($entry) == "")
{
    $message = "Entry required.";
    include('blogPostFail.php');
}


$conn = dbConnect();

$sql = "UPDATE blog_entry SET TITLE = $title, ENTRY = $entry WHERE ID = $blogId";

if($conn->query($sql) == TRUE){
    $message = "Blog updated successfully!";
    include('blogResponse.php');
}
else{
    $message = "Error: " . $sql . "<br>" . $conn->error;
    include ('blogPostFail.php');
}
$conn->close();
?>
</body>
</html>