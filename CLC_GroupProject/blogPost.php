<?php

require_once "myfuncs.php";
include "header.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Post Successful</title>
</head>
<style>
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 14px 25px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<?php


$title = $_POST["Title"];
$entry = $_POST["Entry"];
$userId = getUserId();

if($title == NULL || trim($title) == "")
{
    $message = "Title required.";
    include('blogPostFail.php');
}
if($entry == NULL || trim($entry) == "")
{
    $message = "Entry required.";
    include('blogPostFail.php');
}


$conn = dbConnect();

$sql = "INSERT INTO blog_entry (TITLE, ENTRY, USER_ID) VALUES ('" . $title . "' , '" . $entry . "' , '" . $userId . "')";

if($conn->query($sql) == TRUE){
    $message = "Blog submitted successfully!";
    include('blogResponse.php');
}
else{
    $message = "Error: " . $sql . "<br>" . $conn->error;
    include ('blogPostFail.php');
}
$conn->close();
?>
<br>
<a href="index.html">Home</a>
<a href="blogPost.html">New Entry</a>
</body>
</html>