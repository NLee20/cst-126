<?php

require_once "myfuncs.php";
include "header.php";
include "_menu.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Blogs</title>
</head>
<style>
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 14px 25px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<table>
    <tr>

    </tr>


    <?php
    $conn = dbConnect();
    $sql = "SELECT ID, TITLE FROM blog_entry";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()){
        ?>
        <tr>
            <td>
    <a href="blogPage.php?id=<?=$row["ID"]?>"><?=$row["TITLE"]?></a>
            </td>
        </tr>
        <?php
    }
    $conn->close();
    ?>


</table>

</body>
</html>