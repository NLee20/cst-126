<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 3/19/17
 * Time: 8:47 PM
 */
require_once "myfuncs.php";
include "header.php";
include "_menu.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Blog Deleted</title>
</head>
<style>
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 14px 25px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<p>Blog deleted successfully!</p>
<?php
$userId = getUserId();
$blogId = $_GET["ID"];
$conn = dbconnect();
$sql = "DELETE FROM blog_entry
WHERE ID = $blogId";
$conn->close();
?>
</body>
</html>