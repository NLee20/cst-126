<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog Page</title>
</head>
<style>
    body{
        background-color: darkorchid;
    }
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 14px 25px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 3/12/17
 * Time: 11:17 PM
 */
require_once "myfuncs.php";
include "displayBlog.php";
include "_menu.php";

$id = $_GET["id"];
$conn = dbConnect();

//see users functions from activity5
$sql = "SELECT TITLE, ENTRY, USER_ID FROM blog_entry WHERE ID = $id";

$result = $conn->query($sql);

$index=0;
$blog = array();

$row = $result->fetch_assoc();



if($result->num_rows==1){
    $title = $row["TITLE"];
    $entry = $row["ENTRY"];
    include ('displayBlog.php');
}



$conn->close();

?>
<br>
<button onclick="<?=delete()?>">Delete</button>

<script>
    <?
    function delete(){
        $id = $_GET["id"];
        $conn = dbConnect();
        $sql = "DELETE FROM blog_entry WHERE ID = $id";
        $conn->close();
    }
    ?>
</script>
</body>
</html>
