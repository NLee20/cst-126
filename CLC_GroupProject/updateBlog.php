<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 3/19/17
 * Time: 8:26 PM
 */
require_once "myfuncs.php";
include "header.php";
include "_menu.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Update Blog</title>
</head>
<style>
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 14px 25px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<!--

-->
<form action="updateHandler.php" method="post">
    <p>Please enter old data to keep it.</p>
    Title:
    <br>
    <input type="text" name="Title" minlength="5" maxlength="50"/>
    <br>
    Blog Entry:
    <br>
    <textarea name="Entry" rows="5" cols="100" maxlength="1000"/>
    </textarea>
    <br>
    <input name="Submit" type="submit"/>
</form>

<br>
<br>
<br>
<?php

$id = $_GET["id"];
$conn = dbConnect();


$sql = "SELECT TITLE, ENTRY, USER_ID FROM blog_entry WHERE ID = $id";

$result = $conn->query($sql);

$index=0;
$blog = array();

$row = $result->fetch_assoc();



if($result->num_rows==1){
    $title = $row["TITLE"];
    $entry = $row["ENTRY"];
    include ('displayBlog.php');
}



$conn->close();

?>
</body>
</html>