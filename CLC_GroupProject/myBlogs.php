<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 3/19/17
 * Time: 6:03 PM
 */
require_once "myfuncs.php";
include "header.php";
include "_menu.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>My Blogs</title>
</head>
<style>
    a:link, a:visited {
        background-color: #000000;
        color: white;
        padding: 14px 25px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }

    a:hover, a:active {
        background-color: #9b9b9b;
    }
</style>
<body>
<table>
<?php
$userId = getUserId();
$title = $_GET["TITLE"];
$blogId = $_GET["ID"];

$conn = dbConnect();
$sql = "SELECT ID, TITLE FROM blog_entry WHERE USER_ID=$userId";
$result = $conn->query($sql);
while($row = $result->fetch_assoc()){
    ?>
    <tr>
        <td>
            <a href="blogPage.php?id=<?=$row["ID"]?>"><?=$row["TITLE"]?></a>
            <a href="updateBlog.php">Update</a>
            <a href="deleteBlog.php">Delete</a>
        </td>
    </tr>
    <?php
}
$conn->close();
?>
</table>

</body>
</html>
