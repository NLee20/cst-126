<?php
require_once 'myfuncs.php';

//save our form data
//all variables start with "$"
$firstName = $_POST["FirstName"];
$lastName = $_POST["LastName"];
$userName = $_POST["Username"];
$passWord = $_POST["Password"];
$email = $_POST["Email"];
$age = $_POST["Age"];

//Validate ALL data
if($firstName == NULL || trim($firstName) == "")
{
    $message = "First Name is required.";
    include('registerFail.php');
}
elseif($lastName == NULL || trim($lastName) == "")
{
    $message = "Last Name is required.";
    include('registerFail.php');
}
elseif($userName == NULL || trim($userName) == "")
{
    $message = "Username required.";
    include('registerFail.php');
}
elseif($passWord == NULL || trim($passWord) == "")
{
    $message = "Password required.";
    include('registerFail.php');
}
elseif($email == NULL || trim($email) == "")
{
    $message = "Email required.";
    include('registerFail.php');
}
elseif($age == NULL || trim($age) == "" || $age < 13)
{
    $message = "Must be at least 13 years of age.";
    include('registerFail.php');
}


//create connection
$conn = dbConnect();

$sql = "SELECT ID, USERNAME, PASSWORD
        FROM users WHERE " . " BINARY USERNAME='" . $username . "' AND " . " BINARY PASSWORD='" . $password . "'";
$result = $conn ->query($sql);
if($conn->error)
{
    $message = "Error: " . $sql . "<br>" . $conn->error;
    include('registerFail.php');
}
elseif($result->num_rows == 1)
{
    $message = "User already registered";
    include('registerFail.php');
}

//insert HTML form into user table
if(trim($firstName) == "" || trim($lastName) == "" || trim($userName) == "" || trim($passWord) == "" || trim($email) == ""
|| trim($age) == "") {
    $sql = "INSERT INTO users (FIRST_NAME, LAST_NAME, USERNAME, PASSWORD, EMAIL, AGE) VALUES 
('" . $firstName . "', '" . $lastName . "', '" . $userName . "', '" . $passWord . "', '" . $email . "', '  $age  ')";

    if ($conn->query($sql) == TRUE) {
        include('registerResponse.php');
    } else {
        $message = "Error: " . $sql . "<br>" . $conn->error;
        include('registerFail.php');
    }
}
//close the connection
$conn->close();

?>