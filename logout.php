<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 4/10/17
 * Time: 10:52 PM
 */
session_start();
$_SESSION["LOGGED_IN"] = false;
session_unset();
session_destroy();

header("Location: index.php");
?>